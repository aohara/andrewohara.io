import React from 'react'

import Projects from './Projects'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Avatar from '@material-ui/core/Avatar'
import Box from '@material-ui/core/Box'
import { withStyles } from '@material-ui/core/styles'

const styles = {
  toolbar: {
    marginBottom: 20
  },
  avatar: {
    margin: 10,
    width: 100,
    height: 100
  },
  title: {
    marginLeft: 10,
    marginRight: 10
  }
}

class App extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <div>
        <header>
          <AppBar position='static' className={classes.toolbar}>
            <Toolbar>

              <Avatar className={classes.avatar} alt='portrait' src='/profile.jpg' />

              <Typography variant='h3' className={classes.title}>
                <Box fontWeight='fontWeightBold'>
                  Andrew O'Hara
                </Box>
              </Typography>

              <Typography variant='h5'>
                Cloud-Native Application Developer
              </Typography>

            </Toolbar>
          </AppBar>
        </header>

        <Projects />
      </div>
    )
  }
}

export default withStyles(styles)(App)
