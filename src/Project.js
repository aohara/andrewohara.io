import React from 'react'

import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import Chip from '@material-ui/core/Chip'

import { useStyles } from './Style'
import { DownloadButton, GithubButton, GitlabButton, LinkButton } from './Components'

export default function Project ({ children, name, description, tools, link, githubLink, gitlabLink, download }) {
  const classes = useStyles()

  const buttons = []
  if (download) {
    const { text, link } = download
    buttons.push(<DownloadButton key='download' text={text} link={link} />)
  }
  if (link) {
    buttons.push(<LinkButton key='link' link={link} />)
  }
  if (gitlabLink) {
    buttons.push(<GitlabButton key='gitlab' link={gitlabLink} />)
  }
  if (githubLink) {
    buttons.push(<GithubButton key='github' link={githubLink} />)
  }

  const chips = tools ? tools.map(tool => {
    return <Chip key={tool} label={tool} />
  }) : null

  return (
    <Card className={classes.card}>
      <CardHeader
        title={name}
        action={buttons}
        subheader={description}
      />
      <CardContent>{children}</CardContent>
      <CardActions className={classes.chips}>
        {chips}
      </CardActions>
    </Card>
  )
}
