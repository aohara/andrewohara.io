import React from 'react'

import { loadCSS } from 'fg-loadcss'

import IconButton from '@material-ui/core/IconButton'
import GitHubIcon from '@material-ui/icons/GitHub'
import Link from '@material-ui/core/Link'
import PublicIcon from '@material-ui/icons/Public'
import Icon from '@material-ui/core/Icon'
import GetAppIcon from '@material-ui/icons/GetApp'
import Button from '@material-ui/core/Button'

export function DownloadButton ({ text, link }) {
  return (
    <Link href={link}>
      <Button
        color='primary'
        variant='contained'
        startIcon={<GetAppIcon />}
      >
        {text}
      </Button>
    </Link>
  )
}

export function GithubButton ({ link }) {
  return (
    <IconButton>
      <Link href={link} target='_blank'>
        <GitHubIcon />
      </Link>
    </IconButton>
  )
}

export function GitlabButton ({ link }) {
  React.useEffect(() => {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.1.0/css/all.css',
      document.querySelector('#font-awesome-css')
    )
  }, [])

  return (
    <IconButton>
      <Link href={link} target='_blank'>
        <Icon className='fab fa-gitlab' />
      </Link>
    </IconButton>
  )
}

export function LinkButton ({ link }) {
  return (
    <IconButton>
      <Link href={link} target='_blank'>
        <PublicIcon />
      </Link>
    </IconButton>
  )
}
