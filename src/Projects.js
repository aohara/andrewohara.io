import React, { useState } from 'react'
import Project from './Project'

import { makeStyles } from '@material-ui/core/styles'
import { Typography, Button } from '@material-ui/core'
import Lightbox from 'react-image-lightbox'
import Gist from "react-gist";

const useStyles = makeStyles(() => ({
  imageContainer: {
    display: 'flex'
  },
  item: {
    flex: 1
  }
}))

function ImagePreview({ src }) {
  const classes = useStyles()
  const [isOpen, setOpen] = useState(false)

  return (
      <div className={classes.imageContainer}>
        <div className={classes.item} />
        <Button className={classes.item} onClick={() => setOpen(true)}>
          <img alt={src} src={src} height={200 } />
        </Button>
        <div className={classes.item} />
        { isOpen
          ? <Lightbox mainSrc={src} onCloseRequest={() => setOpen(false) } />
          : undefined
        }
      </div>
  )
}

export default function Projects () {
  return (
    <div>
      <Project
        key='about' name='About Me'
        download={{ text: 'Resume', link: 'https://docs.google.com/document/d/17lv4x3Mmt4x2jam3L87p1X9iL9f26M6cQTNByM1hCow/export?format=pdf' }}
        githubLink='https://github.com/oharaandrew314'
        gitlabLink='https://gitlab.com/aohara'
        tools={['Kotlin', 'REST', 'Architecture', 'AWS', 'Serverless', 'TDD', 'Continuous Delivery']}
      >
        <Typography paragraph>
          Technical lead and architect at Camcloud.  I try to give back to the open-source community as much as I can; be it through Medium articles,
          releasing my own AWS and microservice utilities, or contributing to other projects like the venerable <a href="https://github.com/http4k/http4k">http4k</a> framework!
        </Typography>
      </Project>

      <Project
        key='hexagonal-clients'
        name='Adopt new API Versions with Hexagonal Architecture'
        description='Published in betterprogramming.pub'
        link='https://betterprogramming.pub/353e3d8fd64c'
        tools={['Architecture', 'REST']}
      >
        <Typography paragraph>
          One of my architectural tenets is to never make breaking changes to APIs; this often leads to new API versions, which must be adopted to use the latest features.
          In order to make this transition as painless as possible, I reccomend insulating your internal models and logic with the ports-and-adapters concept from Hexagonal Architecture.
          This guide walks you through how to do that with a sample migration.
        </Typography>

        <ImagePreview src='https://miro.medium.com/max/1400/1*0rQpWVYbmkf49ykbHCBanQ.jpeg'/>
      </Project>

      <Project
        key='testable-services'
        name='Write Testable Kotlin Services'
        description='Published in betterprogramming.pub'
        link='https://medium.com/better-programming/e56d0de0b82b'
        tools={['Architecture', 'Testing']}
      >
        <Typography paragraph>
          I think one of my strengths as a developer is my focus on automated end-to-end tests, and my keen desire to use tools that enable it.
          This article will walk you through a sample application designed to maximize the potential of your automated tests and achieve high levels of test coverage.
        </Typography>

        <ImagePreview src='https://miro.medium.com/max/798/1*_X3qQBfkqPc-d5ldVT4mow.jpeg'/>
      </Project>

      <Project key='dynamo-kotlin-module' name='dynamodb-kotlin-module' description='Kotlin data class plugin for the V2 Dynamo DB Mapper' githubLink='https://github.com/oharaandrew314/dynamodb-kotlin-module' tools={['kotlin', 'AWS Dynamo DB']}>
        <Typography paragraph>
          The only way to use the AWS Dynamo DB Mapper with kotlin is with a horrendous java bean: complete with mutable, defaulted properties.
          I put up with this for years, but eventually, enough was enough.
          I came up with an elegant, non-intrusive plugin for the <a href='https://github.com/aws/aws-sdk-java-v2/tree/master/services-custom/dynamodb-enhanced'>V2 dynamodb-enhanced SDK</a>,
          and I'm so happy with the quality-of-life improvement it's granted.
        </Typography>

        <Gist id="e89705ab2ac9cfd983d2138e2142bfdd" />
      </Project>

      <Project key='cheetos' name='Cheetos' description='Steam Achievement Tracker' link='https://cheetos.andrewohara.io' githubLink='https://github.com/oharaandrew314/cheetos-ui' tools={['kotlin', 'AWS Lambda', 'Dynamo DB', 'http4k', 'React.js']}>
        <Typography paragraph>
          The default steam achievement UI leaves a lot to be desired.  I wanted to design a site that's faster, easier to use, and more useful.  My first version is complete, but there are many more features I'd like to bring, such as comparing with friends, or automatically syncing recent achievements.
        </Typography>

        <ImagePreview src='/cheetos.png' />

        <Typography paragraph>
          I haven't had the chance to use kotlin in many of my side projects, so I wanted to make that happen for this project.  But since my budget for personal projects only allows me to use AWS Lambda, that opens me up to massive performance problems when using the JVM.
          To work around that, I (ironically) scaled back on the microservice architecture, to minimize the number of cold starts.  I also deliberately chose libraries to minimize the function size and startup time.
        </Typography>
        <Typography paragraph>
          Choosing the right web server was a challenge, too.  Python has an advantage because there are a few great tools for supporting any WSGI server on Lambda.
          I originally wanted to use my favourite server, javalin; but after much effort trying to integrate it, I eventually came across http4k.  Despite its weird interfact, it has a lot to love: modular serialization and server backends, end-to-end testing without starting up a server...
          But possibly the part that makes it the best fit is it's official AWS Lambda plugin, and ability to run without a server backend, saving on package size.
        </Typography>
      </Project>
      <Project key='mock-aws-java-sdk' name='mock-aws-java-sdk' description='A library for the JVM that lets you mock AWS out of your tests' githubLink='https://github.com/oharaandrew314/mock-aws-java-sdk' tools={['kotlin', 'AWS']}>
        <Typography paragraph>
          I love using acceptance tests to validate my applications; the vast majority of the time I make an update, I don't even bother running it for real once the tests pass.
          But the frustrating part about testing applications with AWS integration is that you usually have to create test implementations of your AWS mediators; at best, the originals never get any test coverage; but at worst, the mocks don't actually behave the same as the real version.
        </Typography>

        <Typography paragraph>
          I've long been a fan of <a href='https://github.com/spulec/moto'>spulec/moto</a>: an AWS emulator written in python.  It integrates nicely into pytest, and it makes it possible to achieve 100% code coverage.
          But there are two problems:
        </Typography>

        <Typography>
          <ol>
            <li>It's a python package</li>
            <li>It's easy to make a mistake with the mock injection; you might accidentally run tests against the AWS account in your env</li>
          </ol>
        </Typography>

        <Typography paragraph>
          I wanted a tool I could easily and safely integrate into my JVM applications, but it just didn't exist.  I've used Dynamo DB Local in several of my past projects but I don't like all the boilerplate required to get it working, and it only supports Dynamo DB.
          So I ended up making my own; I use it extensively at work, and I'm so very happy with the results!
        </Typography>

        <ImagePreview src='/mock.png' />
      </Project>

      <Project key='javalin-test' name='javalin-test' description='An Acceptance Testing tool for the javalin server' githubLink='https://github.com/javalin/javalin-test' tools={['kotlin', 'java', 'javalin']}>
        <Typography paragraph>
          One thing I love about python WSGI apps is how easy it is to get 100% test coverage with their embedded test clients.
          But sometimes kotlin is the better tool for the job, so I wanted a test client I could use with the javalin server.
          There wasn't one available at the time, so I worked with the maintainer of javalin to create one under the javalin org.
        </Typography>

        <Typography paragraph>
          It isn't the most fully featured tool in existince, but it's found a irreplacable and highly valuable spot in my JVM server toolchain.
        </Typography>

        <ImagePreview src='/javalin-test.png' />
      </Project>

      <Project key='ribbit' name='Ribbit' description='A social news site based on Reddit' gitlabLink='https://gitlab.com/ribbit' link='https://ribbit.andrewohara.io' tools={['python', 'falcon', 'AWS Lambda', 'Dynamo DB', 'React.js', 'Material-UI', 'AWS Amplify']}>
        <Typography paragraph>
          A proof-of-concept application made exclusively out of fully-managed serverless services, secured with Google Sign-In.
        </Typography>

        <ImagePreview src='/ribbit.png' />

        <Typography paragraph>
          The front-end is a react application, hosted on AWS Amplify.
          I've completely given up on hosting static websites on S3 and Cloudfront; it's far too much of a pain to provision correctly and invalidate caches on deployment.
          The Amplify Console is truly the architecture of least resistence for this kind of project.
        </Typography>

        <Typography paragraph>
          The backend uses an assortment of serverless microservices, written with python-falcon, run on Lambda, and Hosted by API Gateway.
          Lambda isn't always the optimal solution to handle web traffic, but its generous free tier is more than enough to run my projects, and the results speak for themselves.
          Each endpoint is secured by a lambda authorizer; verifying the google id token, decoding the user id, and passing it on to the context of the target lambda.
        </Typography>

        <Typography paragraph>
          Since the backend is run on Lambda, the natural pairing for data persistence is Dynamo DB; no connection pools to worry about, and the IAM role is the only authorization you need.
          Dynamo has a very limited feature set, and the keys have to be very carefully arranged; but if you use it correctly, it will never let you down.
        </Typography>
      </Project>

      <Project key='website' name='andrewohara.dev' description='Personal Website' gitlabLink='https://gitlab.com/aohara/andrewohara.io' link='https://andrewohara.dev' tools={['React.js', 'Material-UI', 'AWS Amplify']}>
        <Typography paragraph>
          Built on react, styled with Material-UI, and hosted by the AWS Amplify Console;
          designed to showcase my developer portfolio.
        </Typography>
      </Project>
    </div>
  )
}